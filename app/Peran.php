<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peran extends Model
{
    protected $guarded = [];

    public function cast() {
        return $this->belongsToMany(Cast::class);
    }

    public function film() {
        return $this->belongsToMany(Film::class);
    }
}
