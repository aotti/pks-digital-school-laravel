<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    // DEFAULT NAMA TABEL YG DIPILIH ADALAH casts
    // KALO NAMA TABEL DI DB SUDAH SESUAI
    // MAKA TAK PERLU DEKLARASI VARIABLE protected $table
    protected $guarded = ['id'];

    public function perans() {
        return $this->hasMany(Peran::class);
    }
}
