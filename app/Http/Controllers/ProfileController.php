<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     // UNTUK PARAMETER DGN VARIABLE $user WAJIB SESUAI DGN NAMA PARAMETER
     // DI ROUTE web.php AGAR BISA LANGSUNG JALAN FUNGSINYA
    public function index(User $user)
    {
        // PASTIKAN TABEL YG BERELASI ADA VALUE FOREGIN KEY
        // KALAU TIDAK, ISI VARIABLE AKAN NULL
        $profile = $user->profile;
        // $tes = User::find('1');
        // dd($profile->user->name);
        return view('profile.index', compact('profile'));
    }
}
