<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register() {
      return view('register');
    }
    
    public function GetPost(Request $request) {
      $namaDepan = $request['firstName'];
      $namaBlkng = $request['lastName'];
      return view('welcome', compact('namaDepan', 'namaBlkng'));
    }
}
