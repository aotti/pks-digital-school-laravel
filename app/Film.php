<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $guarded = [];

    public function genre() {
        return $this->belongsTo(Genre::class);
    }

    public function kritiks() {
        return $this->hasMany(Kritik::class);
    }

    public function perans() {
        return $this->hasMany(Peran::class);
    }
}
