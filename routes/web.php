<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@Index');
// Route::get('/register', 'AuthController@Register');
// Route::post('/welcome', 'AuthController@GetPost');
Route::get('/data-table', 'IndexController@DataTable');

// middleware('guest') MEMBERI JENIS IZIN 'guest'
// JADI ROUTE TSB BISA DIAKSES OLEH SIAPAPUN

// middleware('auth') MEMBERI JENIS IZIN 'auth'
// JADI PERLU LOGIN UNTUK MENGAKSES ROUTE TERTENTU
Route::group([
  'middleware' => 'guest'
], function() {
  Route::get('/castGuest', 'CastController@indexGuest');
  Route::get('/castGuest/{cast}', 'CastController@showGuest');
});

Route::group([
  'middleware' => 'auth'
], function() {
  Route::get('/cast', 'CastController@index');
  Route::get('/cast/{cast}', 'CastController@show');
  Route::get('/cast/create', 'CastController@create');
  Route::post('/cast', 'CastController@store');
  // JIKA WILDCARD {cast} DISAMAKAN DENGAN NAMA VAR PADA CastController@show
  // MAKA MODEL Cast BISA OTOMATIS MENJALANKAN FUNGSI Cast::find($id)
  // #################################################
  Route::get('/cast/{id}/edit', 'CastController@edit');
  Route::put('/cast/{id}', 'CastController@update');
  Route::delete('/cast/{id}', 'CastController@destroy');
});

// JIKA INGIN ID LANGSUNG BISA DI FIND KE DATABES
// PAKAI NAMA PARAMETER CUSTOM, JIKA TIDAK MAKA PAKAI {id} SAJA
Route::get('/profile/{user}', 'ProfileController@index')->middleware('auth');

Auth::routes();
