<!DOCTYPE html>
<html lang="en" dir="ltr">
@extends('layouts.master')

@section('content')
  <h1> SELAMAT DATANG, {{ $namaDepan .' '. $namaBlkng }}! </h1>
  <h3> Terima kasih telah bergabung di Website kami. Media belajar kita bersama! </h3>
@endsection
