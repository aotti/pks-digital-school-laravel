<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="{{ asset('adminlte/index3.html') }}" class="brand-link">
    <img src="{{ asset('adminlte/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">AdminLTE 3</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">
          @auth
            {{ auth()->user()->name }}
          @else
            Guest
          @endauth
        </a>
      </div>
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
      <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-sidebar">
            <i class="fas fa-search fa-fw"></i>
          </button>
        </div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ asset('adminlte/index.html') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Dashboard v1</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ asset('adminlte/index2.html') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Dashboard v2</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ asset('adminlte/index3.html') }}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Dashboard v3</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-table"></i>
            <p>
              Tables
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/data-table" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Data Tables</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          @auth
            <a href="/cast" class="nav-link">
          @else
            <a href="/castGuest" class="nav-link">
          @endauth
              <i class="nav-icon fas fa-table"></i>
              <p>
                Cast
              </p>
            </a>
        </li>
        <li class="nav-item">
          @auth
            <a href="/profile/{{ auth()->id() }}" class="nav-link">
          @else
            <a href="/profile/0" class="nav-link">
          @endauth
              <i class="nav-icon fas fa-table"></i>
              <p>
                Profile
              </p>
            </a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-table"></i>
            <p>
              Genre
            </p>
          </a>
        </li>
        @auth
          <li class="nav-item">
            <form class="nav-link text-center p-0" action="{{ route('logout') }}" method="post">
              @csrf
              <button type="submit" class="btn btn-danger w-100"> Logout </button>
            </form>
           {{-- <a href="#" class="nav-link text-center bg-danger">
             <p> Logout </p>
           </a> --}}
          </li>
        @else
          <li class="nav-item">
           <a href="{{ route('login') }}" class="nav-link text-center bg-success">
             <p> Login </p>
           </a>
          </li>
          <li class="nav-item">
           <a href="{{ route('register') }}" class="nav-link text-center bg-primary">
             <p> Register </p>
           </a>
          </li>
        @endauth
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
