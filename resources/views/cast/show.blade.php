@extends('layouts.master')

@section('title', 'Detail Data Cast')
@section('content')
  <h2>Show Post {{$cast->id}}</h2>
  <h4>{{$cast->nama}}</h4>
  <p>Umur {{$cast->umur}}</p>
  <p>Bio: {{$cast->bio}}</p>
@endsection
