@extends('layouts.master')

@section('title', 'Form Tambah Cast')
@section('content')
  <section class="content">
    <h2> Tambah Data Cast </h2>
    <form action="/cast" method="POST">
       @csrf
       <div class="form-group">
           <label for="nama"> Nama </label>
           <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama">
           @error('nama')
               <div class="alert alert-danger">
                   {{ $message }}
               </div>
           @enderror
       </div>
       <div class="form-group">
           <label for="umur"> Umur </label>
           <input type="text" class="form-control" name="umur" id="umur" maxlength="2" placeholder="Masukkan Umur">
           @error('umur')
               <div class="alert alert-danger">
                   {{ $message }}
               </div>
           @enderror
       </div>
       <div class="form-group">
           <label for="bio"> Bio </label> <br>
           <textarea name="bio" id="bio" rows="8" cols="80" maxlength="255"></textarea>
           @error('bio')
               <div class="alert alert-danger">
                   {{ $message }}
               </div>
           @enderror
       </div>
       <button type="submit" class="btn btn-primary"> Tambah </button>
    </form>
  </section>
@endsection
