<!DOCTYPE html>
<html lang="en" dir="ltr">
@extends('layouts.master')

@section('content')
  <h1> Buat Account Baru </h1>
  <form action="/welcome" method="post">
    @csrf
    <h4> Sign Up Form </h4>
    <label for=""> First Name: </label> <br>
    <input type="text" name="firstName" value=""> <br><br>
    <label for=""> Last Name: </label> <br>
    <input type="text" name="lastName" value=""> <br><br>

    <label for=""> Gender </label> <br>
    <input type="radio" name="" value="">
    <label for=""> Male </label> <br>
    <input type="radio" name="" value="">
    <label for=""> Female </label> <br><br>

    <label for=""> Nationality </label> <br>
    <select name="">
      <option value=""> Indonesia </option>
      <option value=""> Amerika </option>
      <option value=""> Inggris </option>
    </select> <br><br>

    <label for=""> Language Spoken </label> <br>
    <input type="checkbox" name="" value="">
    <label for=""> Bahasa Indonesia </label> <br>
    <input type="checkbox" name="" value="">
    <label for=""> English </label> <br>
    <input type="checkbox" name="" value="">
    <label for=""> Other </label> <br><br>

    <label for=""> Bio </label> <br>
    <textarea name="name" rows="8" cols="40"></textarea> <br>
    <input type="submit" name="" value="Sign Up">
  </form>
@endsection
