<!DOCTYPE html>
<html lang="en" dir="ltr">
@extends('layouts.master')

@section('title', 'Profile Index')
@section('content')
  <section class="content">
    <table class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nama</th>
          <th scope="col">Umur</th>
          <th scope="col">Bio</th>
          <th scope="col">Actions</th>
        </tr>
      </thead>
      <tbody>
          <tr>
            <td> # </th>
            <td>{{$profile->user->name}}</td>
            <td>{{$profile->umur}}</td>
            <td>{{$profile->bio}}</td>
            <td>
                <a href="/cast/{{$profile->id}}" class="btn btn-info">Show</a>
                <a href="/cast/{{$profile->id}}/edit" class="btn btn-primary">Edit</a>
                <form action="/cast/{{$profile->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                </form>
            </td>
          </tr>
      </tbody>
    </table>
  </section>
@endsection
